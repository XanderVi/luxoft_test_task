import sqlite3


CREATE_TABLE = '''CREATE TABLE IF NOT EXISTS result_files
                  ([id] INTEGER PRIMARY KEY,
                   [path_to_source_data] text,
                   [path_to_result_file] text,
                   [size_of_result_file] integer)'''

with sqlite3.connect('integration.db') as conn:
    cursor = conn.cursor()
    cursor.execute(CREATE_TABLE)
