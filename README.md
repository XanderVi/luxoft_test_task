# How to start (with just 2 simple steps)

At first, you should install the requirements (pandas library for csv files):

```bash
pip install -r requirements.txt (or pip3 instead of pip)
```
After that you should create an empty SQLite database with the right table (also empty):
```bash
python create_db.py (or 'python3 create_db.py' if 'python' means 'python2' in your OS)
```

# Usage

```bash
python script.py <id> (or 'python3 script.py <id>')

Example:
python script.py 456
```