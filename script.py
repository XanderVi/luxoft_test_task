"""
Maybe using pandas for this task is overengineering and
standard csv module could be used instead of it. Something like:
import csv
with open('data.csv', 'r') as data_object:
    data = dict(csv.reader(data_object, delimiter=','))
"""

import os
import sys
import time
import sqlite3
import subprocess
import pandas as pd


# Creating a directory if we haven't created it earlier
if 'result_data' not in os.listdir('../'):
    subprocess.Popen('mkdir ../result_data', shell=True)

data = pd.read_csv('data.csv')

try:
    # in line 'python script.py <id>' - script.py is sys.argv[0], <id> is sys.argv[1]
    id = int(sys.argv[1])
except IndexError:
    print('You have to write id explicitly (Example: python script.py 456)')
    exit()
except ValueError as e:
    wrong_value = str(e).split(':')[1]
    print(f'id should be an integer, not {wrong_value}')
    exit()

result = data.loc[data['id'] == id]

if result.empty:
    raise KeyError(f'There is no such id as {id}. Try another one.')
else:
    result = result.T.squeeze()  # convert DataFrame to pandas Series
    dir_name = result.at["name"]

    source_path = os.path.abspath(f'data_for_integration/{dir_name}')
    subprocess.Popen(f'cp -r data_for_integration/{dir_name} ../result_data', shell=True)
    result_file_path = '/'.join(source_path.split('/')[:-3]) +\
                       f'/result_data/{dir_name}/data.txt'

    time.sleep(0.5)  # waiting while file will be written to the result_file_path
    file_size = os.path.getsize(result_file_path)

    ADD_INFO_TO_DB = f'''INSERT INTO result_files
                         (path_to_source_data,
                          path_to_result_file,
                          size_of_result_file)
                          VALUES ('{source_path}', '{result_file_path}', {file_size})'''

    with sqlite3.connect('integration.db') as conn:
        cursor = conn.cursor()
        cursor.execute(ADD_INFO_TO_DB)
